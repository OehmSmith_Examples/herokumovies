# Tutorial - Deploy Typescript NodeJS Express app to Heroku

Working from https://amenallah.com/node-js-typescript-jest-express-starter/ as the base app

## Install typescript

Most tutorials or even the official documentation in https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html say to install typescript globally:

     > npm install -g typescript
     
Heroku doesn't have a global install of typescript so it needs to be kept locally.  The example project does just this:

     > npm i -D nodemon rimraf typescript ts-node ts-jest jest @types/jest @types/node
     
## @types/node

In the case you have pinned your `@types/node` at an older version you will see something like this error:

    ~/AppData/Roaming/nvm/v11.15.0/node_modules/typescript/lib/lib.es2015.iterable.d.ts:41:6 - error TS2300: Duplicate identifier 'IteratorResult'.
    
    41 type IteratorResult<T, TReturn = any> = IteratorYieldResult<T> | IteratorReturnResult<TReturn>;
            ~~~~~~~~~~~~~~
    
      node_modules/@types/node/index.d.ts:170:11
        170 interface IteratorResult<T> { }
                      ~~~~~~~~~~~~~~
        'IteratorResult' was also declared here.
    
    node_modules/@types/node/index.d.ts:170:11 - error TS2300: Duplicate identifier 'IteratorResult'.
    
    170 interface IteratorResult<T> { }
                  ~~~~~~~~~~~~~~
    
    ~/AppData/Roaming/nvm/v11.15.0/node_modules/typescript/lib/lib.es2015.iterable.d.ts:41:6
        41 type IteratorResult<T, TReturn = any> = IteratorYieldResult<T> | IteratorReturnResult<TReturn>;
                ~~~~~~~~~~~~~~
        'IteratorResult' was also declared here.
    
    
    Found 2 errors.

From https://stackoverflow.com/questions/57331779/typescript-duplicate-identifier-iteratorresult.  And as per that you need to update your version of `@types/node`.  This was a problem I struck as I was working with older code and wanted to include this discussion of it.

## Update port for cloud service

Change the `index.ts` to contain this instead since it originally hard-coded port 5000:

    app.listen(process.env.PORT, () => {
        console.log(`server started on port ${process.env.PORT}`)
    });

## Dummy data

I added this constructor to the `MoviesApi.ts`:

    constructor() {
        // setup some dummy data
        movies.push({
            name: 'Pirates of the caribbean',
            rating: 8.5
        })
        movies.push({
            name: 'Star Wars: A new hope',
            rating: 8.7
        })
    }

## Heroku

Now deploy to Heroku

1. Setup an account if you don't already have one and create an app on Heroku
2. In your terminal:


    heroku login
    heroku create moviesheroku

3. You may need to add the returned git url as a remote (check with `git remote -v`):

    
    git remote add heroku <git url>

3. Lookup or search for buildpacks with:

* Lookup at: https://devcenter.heroku.com/articles/buildpacks#officially-supported-buildpacks
* Search with:


    heroku buildpacks:search typescript

4. Add buildpacks:


    heroku buildpacks:add zidizei/typescript
    heroku buildpacks:add heroku/nodejs
    
5. Confirm buildpacks:


    heroku buildpacks

6. Commit to your local repository

    git init  // if not already done
    git add --all
    git ci -m "Initial commit.  Test project all setup and should be ready to 'serve' but not yet ready to deploy to heroku"
    
7. Run with

    `npm dev`
    
8. Test with postman or similar or run this from the command-line:

    curl http://localhost:5000/movies
    
7. Test with `npm run build`
8. Update the npm scripts so that after installation (`npm install`) on Heroku, it will build it before attempting to `npm run start`


    "postinstall": "npm run build

9. Commit to local repository:


    git add --all
    git ci -m "Now it should deploy, build and run on heroku"

9. Deploy to heroku.  It should build and start up.


    git push heroku master

10. Test (assuming the app you `heroku create`d is `moviesheroku` - adjust accordingly)


    curl https://moviesheroku.herokuapp.com/movies

## Variations

### Procfile

I have specified no Procfile telling heroku anything about the app.  Fortunately it is able to determine that this is a node npm app and to start it with `npm start`.  However you can explicitly define this and will need to if you have multiple apps or similar.  You could add a Procfile to contain:


    web: npm start

### Node and NPM version

Heroku also defaults to using one of the most recent versions of this.  You could explicitly set the versions at the top-levl in the `package.json` file like:


     "engines": {
         "node": "10.x",
         "npm": "6.x"
     },

Although if you don't specify a `npm` version then Heroku will use a sensible default for the version of node.
 
